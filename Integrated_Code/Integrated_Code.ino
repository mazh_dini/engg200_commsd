#include <LU.h>       // Include Comms C Helper Class (Provided by Comms A)
#include <Motion.h>   // Include Comms D Helper Class (Provided by Comms A)
#include <Sensor.h>   // Include Comms D Sensor Class (Provided by Comms D)
#include <Servo.h>    // Include Servo class for help with Motion control (Provided by Comms D)

// Comms A Setup
unsigned char inv[6] = {4,4,4,4,4,4};
Motion m;
LU l;
// Comms D Setup
unsigned int moveRobot();
Sensor canSensor(1, 2, 3, 4, 5); // CHANGE PIN LOCATIONS
Sensor lineSensor(6, 7, 8, 9, 10); // CHANGE PIN LOCATIONS
Servo myservoLeft;
Servo myservoRight;

void setup() {

  // Comms D Motion Setup - CHANGE PIN LOCATIONS
  attachInterrupt(digitalPinToInterrupt(18), ISRleft, RISING);
  attachInterrupt(digitalPinToInterrupt(19), ISRright, RISING);
  pinMode(22,INPUT); // 18 and 22 come from left
  pinMode(23,INPUT); // 19 and 23 come from right
  myservoLeft.attach(4);  // attaches the servo on pin 4 to the servo object
  myservoRight.attach(5);  // attaches the servo on pin 5 to the servo object

}

void loop() {

    // RED
    if (l.getRedNeed() > 0) {

        if (inv[0] > 0) { // Check inventory for RED1
            m.setTargetLocation(RED1);
            m.setStatus(MOTION);

            moveRobot();

            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setRedNeed(l.getRedNeed() - l.getCount());//??
                inv[0] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

        if (l.getRedNeed() > 0 && inv[1] > 0) { // Check inventory for RED2
            m.setTargetLocation(RED2);
            m.setStatus(MOTION);

            moveRobot();

            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setRedNeed(l.getRedNeed() - l.getCount());//??
                inv[1] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

    }

    // GREEN
    if (l.getGreenNeed() > 0) {

        if (inv[2] > 0) {
            m.setTargetLocation(GREEN1);
            m.setStatus(MOTION);

            moveRobot();

            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setGreenNeed(l.getGreenNeed() - l.getCount());//??
                inv[2] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

        if (l.getRedNeed() > 0 && inv[3] > 0) {
            m.setTargetLocation(GREEN2);
            m.setStatus(MOTION);
            moveRobot();
            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setGreenNeed(l.getGreenNeed() - l.getCount());//??
                inv[1] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

    }

    // BLUE
    if (l.getBlueNeed() > 0) {

        if (inv[4] > 0) {
            m.setTargetLocation(BLUE1);
            m.setStatus(MOTION);

            moveRobot();

            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setBlueNeed(l.getBlueNeed() - l.getCount());//??
                inv[4] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

        if (l.getBlueNeed() > 0 && inv[5] > 0) {
            m.setTargetLocation(BLUE2);
            m.setStatus(MOTION);

            moveRobot();

            if (m.getStatus() == MHALT) {
                l.setStatus(PICK);
                //input loading team
                //return the number picked up as red
                // x is an amount that the other team collected
                l.setBlueNeed(l.getBlueNeed() - l.getCount());//??
                inv[5] -= l.getCount();
                l.setCount(0);
                l.setStatus(HALT);
            }
        }

    }

    if (l.getTotal() == 0) {

        m.setTargetLocation(END);
        m.setStatus(MOTION);

        moveRobot();

        if (m.getStatus() == MHALT && m.getLocation() == END) {
            l.setStatus(DROP);
        }

    }

}

void moveRobot() {

    // Return control to Comms A if status MHALT
    if(m.getStatus() == 0)
      return 1;

    // Return control to Comms A
    if(m.getLocation() == m.getTargetLocation()) {

      // Disable Sensor Reading
      canSensor.setStatus(0);
      // Stop Robot Motion
      myservoLeft.write(90);
      myservoRight.write(90);
      // Set Status to MHALT
      m.setStatus(MHALT);
      // Return Successfully
      return 0;

    }

    // Expected can stations will be measured and hard coded when the robot is tested
    // int target = canLocations[targetLocation] - canLocations[currentLocation];

    // Prevents Sensor from reading the same color again at a location
    if(canSensor.getCanSensorData() == 3)
      canSensor.setStatus(1); // Set true if no color detected

    // Function is expected to perform like a loop using the void loop instead with a series of if statements
    if(m.getLocation() != m.getTargetLocation()) {

        if(canSensor.getStatus() == 1 && getCanSensorData() != 3) {

            if(m.getLocation() == 6)
              m.setLocation(0);
            else
              m.setLocation(m.getLocation()++);

            canSensor.setStatus(0);

        }

        // Set targetLocation to start if instructions are to move backwards
        if(m.getTargetLocation() < m.getLocation())
            m.setTargetLocation(START);

        // TO BE COMPLETED - Implement PID Controller and tune values

        int throttleLeft = (int) 90.0 - 0.9 * (target-countLeft);
        int throttleRight = (int) 90.0 + 1.3 * (target-countRight);

        // Adjust Robot to Black and White tape
        int diff = canSensor.getLineSensorData();

        if(diff > greyScaleConstant) {
          myservoRight.write(90 + (10  * (throttleLeft / |throttleLeft|)));
        } else {
          myservoLeft.write(90 - (10  * (throttleLeft / |throttleLeft|)));
        }

        // Move Robot
        myservoLeft.write(constrain(throttleLeft, 0, 180));
        myservoRight.write(constrain(throttleRight, 0, 180));

    }

    // Return control to Comms A
    return 0;

}
