#pragma once
#ifndef Sensor_h
#define Sensor_h

#include "Arduino.h"

/*
  Sensor being used is provided by the motions team
  Model: XC3708 Link: https://www.jaycar.com.au/arduino-compatible-colour-sensor-module/p/XC3708
*/

class Sensor {
  public:
    Sensor(int s0, int s1, int s2, int s3, int sensorOut);
    int getLineSensorData();
    unsigned char getCanSensorData();

    void setStatus(unsigned char status);
    unsigned char getStatus();
  private:
    // Sensor Setup Variables
    unsigned char _s0;
    unsigned char _s1;
    unsigned char _s2;
    unsigned char _s3;
    unsigned char _sensorOut;
    int _R;
    int _G;
    int _B;
    unsigned char _allowSensorReading;
};

#endif
