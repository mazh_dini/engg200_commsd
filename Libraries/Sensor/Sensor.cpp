#include "Arduino.h"
#include "Sensor.h"

Sensor::Sensor(int s0, int s1, int s2, int s3, int sensorOut) {

  _s0 = s0;  _s1 = s1; _s2 = s2; _s3 = s3; _sensorOut = sensorOut;
  _R = 0; _G = 0; _B = 0; _allowSensorReading = 1; // set to true

  // Sensor Initlisation - Can Color Station
  pinMode(_s0, OUTPUT);
  pinMode(_s1, OUTPUT);
  pinMode(_s2, OUTPUT);
  pinMode(_s3, OUTPUT);
  pinMode(_sensorOut, INPUT);

  // Set sensor to 20% frequency
  digitalWrite(_s0, HIGH);
  digitalWrite(_s1, LOW);

}

unsigned char Sensor::getCanSensorData() {

  // Read in red color
  digitalWrite(_s2, LOW);
  digitalWrite(_s3, LOW);
  _R = pulseIn(_sensorOut, LOW);
  _R = map(_R, 25, 70, 255, 0);

  // Read in green color
  digitalWrite(_s2, HIGH);
  digitalWrite(_s3, HIGH);
  _G = pulseIn(_sensorOut, LOW);
  _G = map(_G, 25, 70, 255, 0);

  // Read in blue color
  digitalWrite(_s2, LOW);
  digitalWrite(_s3, HIGH);
  _B = pulseIn(_sensorOut, LOW);
  _B = map(_B, 25, 70, 255, 0);

  if (R > 0 && R < 30 && G > 40 && G < 255 && B > 8 && B < 255)
    return 0; // Color is RED
  else if (R > 25 && R < 42 && G > 20 && G < 40 && B > 9 && B < 14)
    return 1; // Color is GREEN
  else if (R > 42 && R < 80 && G > 40 && G < 65 && B > 12 && B < 18)
    return 2; // Color is BLUE
  else
    return 3; // Color is NO_COLOR

}

int Sensor::getLineSensorData() {

  // Read in red color
  digitalWrite(_s2, LOW);
  digitalWrite(_s3, LOW);
  _R = pulseIn(_sensorOut, LOW);
  _R = map(_R, 25, 70, 255, 0);

  // Read in green color
  digitalWrite(_s2, HIGH);
  digitalWrite(_s3, HIGH);
  _G = pulseIn(_sensorOut, LOW);
  _G = map(_G, 25, 70, 255, 0);

  // Read in blue color
  digitalWrite(_s2, LOW);
  digitalWrite(_s3, HIGH);
  _B = pulseIn(_sensorOut, LOW);
  _B = map(_B, 25, 70, 255, 0);

  return (_R + _G + _B) / 3;

}

void Sensor::setStatus(unsigned char status) {
  _allowSensorReading = status;
}
unsigned char Sensor::getStatus() {
  return _allowSensorReading; // 0 ==  false, 1 == true
}
